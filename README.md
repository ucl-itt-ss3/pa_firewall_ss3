# Packer Palo-alto build v. 1.0.0
--------------------------------------------------------------------------------
*   This document includes:
    *   brief description of the system. 
    *   An installation guide.
    *   Version state


# Description

This build allows a user to quickly despatch a Palo Alto, Virtual Mashine. The VM has three zones and a MGMT network(Specified in the matrix below). The firewall has a SU user "test" with a password of "test".
The firewall version is 8.1.3. 


| Name | assigned IP address  | Interface |
| ---- | ---------- | ----------- |
| MGMT  | DHCP |   MGMT-Interface  |
| Untrust / Internet    | 192.168.1.1/24 |   Eth1/1  |
| DMZ   | 10.10.0.1/30 |   Eth1/2  |
| Trust / Internal  | 192.168.10.20 |   Eth1/3  |

# Installation guide

!It is required to accuire the installation files from the creator of this build (.ISO can be downloaded here: https://www.jottacloud.com/s/1565b725a50a8174bd9a95bff76c496f335) And download the .json file from the Packer repository!

!If using a new default palo alto VMX file change all interfaces(VMnets) to vmxnet3, otherwise you will get an error when booting the PA!

To install this build follow the steps below.
* Install packer on you OS. See https://www.packer.io/ for guidelines on this.
* Open the downloaded .json file, ad replace the three paths, and save it:
    *   Path to ISO image
    *   Path to WMX file
    *   Path to install directory
* Make sure that Packer has an enviromental variable set if running Winodws. 
* run the command "packer build C:\path to the .json file\PA_test_enviroment.json" in CMD/Terminal

# Version description

| Version | Changes  |
| ---- | ---------- |
| 1.0.0 | None |



